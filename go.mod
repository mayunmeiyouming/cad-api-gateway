module gitlab.com/mayunmeiyouming/cad-api-gateway

go 1.14

require (
	github.com/99designs/gqlgen v0.11.3
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/golang/protobuf v1.4.2
	github.com/prometheus/common v0.11.1
	github.com/rs/cors v1.7.0
	github.com/vektah/gqlparser/v2 v2.0.1
	gitlab.com/mayunmeiyouming/cad-micro v0.0.0-00010101000000-000000000000
	google.golang.org/protobuf v1.25.0
)

// replace gitlab.com/mayunmeiyouming/cad-micro => /home/pacman-09/workspace/cad-micro

replace gitlab.com/mayunmeiyouming/cad-micro => /home/pacman-10/桌面/cad-micro

// replace gitlab.com/mayunmeiyouming/cad-micro => /home/hw/cad-micro
